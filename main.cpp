/* 
 * File:   main.cpp
 * Author: Janis
 *
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#define SIZE 10000

using namespace std;

class QueueClass {
    int queue[SIZE];
    int head, tail;
public:
    QueueClass();
    void q(int num);
    int deq();
};

QueueClass::QueueClass() {
    head = tail = 0;
}

void QueueClass::q(int num) {
    if (tail + 1 == head || (tail + 1 == SIZE && !head)) {
        cout << "Queue is full\n";
        return;
    }
    tail++;
    if (tail == SIZE) tail = 0; // cycle around
    queue[tail] = num;
}

int QueueClass::deq() {
    if (head == tail) {
        //cout << "Queue is empty\n";
        return 0; // or some other error indicator
    }
    head++;
    if (head == SIZE) head = 0; // cycle around
    return queue[head];
}

struct TreeNode {
    int item; // The data in this node.
    TreeNode *left; // Pointer to the left subtree.
    TreeNode *right; // Pointer to the right subtree.
};

TreeNode* createNode(int item) {
    TreeNode *ret = new TreeNode;
    ret->right = ret->left = NULL;
    ret->item = item;
    return ret;
}

TreeNode* preorder(TreeNode *tree, int data) {
    if (tree != NULL) {
        if (tree->item == data) {
            return tree;
        }
        TreeNode* left = preorder(tree->left, data);
        TreeNode* right = preorder(tree->right, data);
        if (right != NULL) {
            return right;
        } else if (left != NULL) {
            return left;
        } else {
            return NULL;
        }
    }
    return NULL;
}

class stack {
public:

    int a[SIZE];
    int tos; // Top of Stack

    stack();
    void push(int);
    int pop();
    int isempty();
    int isfull();
};

stack::stack() {
    tos = 0; //Initialize Top of Stack
}

int stack::isempty() {
    return (tos == 0 ? 1 : 0);
}

int stack::isfull() {
    return (tos == SIZE ? 1 : 0);
}

void stack::push(int i) {
    if (!isfull()) {
        a[tos] = i;
        tos++;
    } else {
        cout << "Stack overflow error !Possible Data Loss !";
    }
}

int stack::pop() {
    if (!isempty()) {
        return (a[--tos]);
    } else {
        cout << "Stack is empty! What to pop...!";
    }
    return 0;
}

bool addNode(stack *steks, int parentInt, TreeNode *&root) {

    if (root == NULL) {

        root = createNode(parentInt);
    }
    while (!steks->isempty()) {

        TreeNode* parent = preorder(root, parentInt);

        if (parent->left == NULL) {
            parent->left = createNode(steks->pop());

        } else {
            for (TreeNode *p = parent->left; p != NULL; p = p->right) {
                if (p->right == NULL && !steks->isempty()) {

                    p->right = createNode(steks->pop());
                }
            }

        }
    }
    return true;

}

bool drukat(TreeNode *root, fstream &fout) {

    QueueClass* rinda = new QueueClass;
    fout << root->item << " ";
    for (TreeNode *p = root->left; p != NULL; p = p->right) {
        if (p->left != NULL) {
            rinda->q(p->item);
        }

        fout << p->item << " ";
    }

    fout << "\n";
    int nextElem = rinda->deq();
    while (nextElem != 0) {
        drukat(preorder(root, nextElem), fout);
        nextElem = rinda->deq();
    }

    return true;
}

int main() {
    ifstream myfile;
    fstream fout;
    TreeNode *root = NULL;
    myfile.open("alise.in");
    fout.open("alise.out", ios::out);
    stack *steks = new stack;
    int hold, first;
    //QueueClass *rinda = new QueueClass;
    hold = first = 0;
    while (!myfile.eof()) {
        int myInt;

        myfile >> myInt;
        if (hold == 0) {
            first = myInt;
            hold++;
        } else {
            steks->push(myInt);
        }
        while (myInt != 0) {

            int ch = myfile.peek();

            if (ch == ' ' || ch == '\t' || ch == '\r') {
                char ch2;
                myfile.read(&ch2, 1);
            } else if (ch == '\n') {
                char ch2;
                myfile.read(&ch2, 1);
                addNode(steks, first, root);
                hold--;

                
            } else {
                break;
            }
        }
    }
    drukat(root, fout);
    fout << "0" << "\n";
    myfile.close();
    return 0;
}

